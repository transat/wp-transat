# Plug-ins & Themes for Transat website (WordPress)

This repository contains plugins and themes developed for 
[Transat website](https://transat-asso.fr/) powered by
[WordPress](https://wordpress.com/).

## Directory organization

The repository is organized into two main directories:

* `plugins/`: utility source code for peculiar behavior.
* `themes/`: Templates and Themes.

Each theme or plugin comes in a sub-directory.

## Release & Deployment

(To be specified)

## License

The source code of this project shall be used accordingly
to [GNU AGPL 3.0 License](LICENSE).